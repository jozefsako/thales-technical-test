package com.example.elevators.exceptions;

public class MinFloorException extends Exception {
    public static final String MESSAGE = "Elevator cannot move below the minimum floor.";

    public MinFloorException() {
        super(MESSAGE);
    }
}