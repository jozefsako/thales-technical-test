package com.example.elevators.exceptions;

public class MaxFloorException extends Exception {
    public static final String MESSAGE = "Elevator cannot move above the maximum floor.";

    public MaxFloorException() {
        super(MESSAGE);
    }
}