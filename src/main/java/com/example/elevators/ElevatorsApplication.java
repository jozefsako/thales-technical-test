package com.example.elevators;

import com.example.elevators.enums.Direction;
import com.example.elevators.exceptions.MaxFloorException;
import com.example.elevators.exceptions.MinFloorException;
import com.example.elevators.models.Building;
import com.example.elevators.models.FloorRequest;
import com.example.elevators.utils.Formatter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class ElevatorsApplication {

	static final Scanner scanner = new Scanner(System.in);
	static Building building = Building.getInstance();
	static Formatter formatter = Formatter.getInstance();

	public static void main(String[] args) {
		SpringApplication.run(ElevatorsApplication.class, args);

		while(true) {
			displayMenu();
			int choice = scanner.nextInt();
			int floorNumber;
			int elevator = 0;

			switch (choice) {
				case 1:
					System.out.print("\nWhat floor: ");
					floorNumber = scanner.nextInt();
					Direction direction = getFloorAction(floorNumber);
					FloorRequest floorRequest = new FloorRequest(floorNumber, direction, false);

					try {
						building.requestElevator(floorRequest);
					} catch (MinFloorException | MaxFloorException e) {
						e.printStackTrace();
					}
					break;
				case 2:
					System.out.print("\nSelect Elevator: ");
					elevator = scanner.nextInt();
					System.out.print("\nSelect Floor: ");
					floorNumber = scanner.nextInt();
					try {
						building.getListElevators().get(elevator).dispatchElevator(floorNumber, false);
					} catch (MinFloorException | MaxFloorException e) {
						e.printStackTrace();
					}
					break;
				case 3:
					formatter.displayBuilding(building);
					break;
				case 5:
					System.exit(0);
				default:
					displayMenu();
			}
		}
	}

	public static void displayMenu() {
		System.out.println("--------------------------------");
		System.out.println("1) Request Elevator");
		System.out.println("2) Dispatch Elevator  ");
		System.out.println("3) Show Elevators  ");
		System.out.println("5) EXIT                         ");
		System.out.println("--------------------------------");
	}

	public static Direction getFloorAction(int floorRequest) {
		System.out.println("--------------------------------");
		if (floorRequest == 0 ){
			System.out.println("1) UP");
		}
		else if (floorRequest == (building.getFloors()-1)) {
			System.out.println("2) DOWN  ");
		} else {
			System.out.println("1) UP");
			System.out.println("2) DOWN  ");
		}
		System.out.println("--------------------------------");

		System.out.println("Votre choix: ");

		int choice = scanner.nextInt();

		switch (choice) {
			case 1: return Direction.UP;
			case 2: return Direction.DOWN;
			default: return getFloorAction(floorRequest);
		}
	}
}
