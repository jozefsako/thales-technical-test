package com.example.elevators.utils;

import com.example.elevators.models.Building;
import com.example.elevators.models.Elevator;
import org.springframework.stereotype.Component;

@Component
public final class Formatter {

    private static Formatter INSTANCE;
    private Formatter() { }

    public static Formatter getInstance(){
        if (INSTANCE == null) {
            INSTANCE = new Formatter();
        }
        return INSTANCE;
    }

    public void displayBuilding(Building building) {
        System.out.println(displayFloors(building.getFloors()));
        for(Elevator elevator: building.getListElevators()) {
            System.out.println(displayElevator(elevator));
        }
    }

    public String displayElevator(Elevator elevator) {
        String result = "";
        for (int i = 0; i < elevator.getFloors(); i++) {
            if ( i == elevator.getCurrentFloor()) {
                result += " | " + elevator.getCurrentFloor();
            } else {
                result += " | x";
            }
        }
        result += " | - ["+ elevator.getElevatorState().toString();
        result += " - " + elevator.getDirection().toString();
        result += elevator.isEmergency() ? " - EMERGENCY]\n " : "]\n";
        result += separator(elevator.getFloors());
        return result;
    }

    public String displayFloors(int floors) {
        String result = separator(floors) + "\n";
        for (int i = 0; i < floors; i++) {
            result += " | " + i;
        }
        result += " |\n" + separator(floors);
        return result;
    }

    public String separator(int floors) {
        String result = " ";
        for (int i = 0; i < floors; i++) {
            result += "----";
        }
        return result + "-";
    }
}
