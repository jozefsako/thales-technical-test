package com.example.elevators.web;

import com.example.elevators.enums.Direction;
import com.example.elevators.exceptions.MaxFloorException;
import com.example.elevators.exceptions.MinFloorException;
import com.example.elevators.models.Building;
import com.example.elevators.models.FloorRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/emergency")
public class EmergencyController {

    private Building building = Building.getInstance();

    @GetMapping
    public ResponseEntity<String> sayHello() {
        return ResponseEntity.ok("Emergency-Elevator API");
    }

    @GetMapping("/floor-dispatch/{floor}/{elevator}")
    public ResponseEntity<String> dispatchElevator(@PathVariable("floor") int floor, @PathVariable("elevator") int elevator) {
        try {
            building.getListElevators().get(elevator).dispatchElevator(floor, true);
            return ResponseEntity.ok("Emergency detected! dispatched to: " + floor);
        } catch (MinFloorException | MaxFloorException e) {
            return ResponseEntity.badRequest().body("Failed to dispatch emergency..");
        }
    }

    @GetMapping("/floor-request/{floor}/{direction}")
    public ResponseEntity<String> requestElevator(@PathVariable("floor") int floor, @PathVariable("direction") int direction) {

        Direction mappedDirection = direction == 0 ? Direction.DOWN : Direction.UP;
        FloorRequest floorRequest = new FloorRequest(floor, mappedDirection, true);

        try {
            building.requestElevator(floorRequest);
            return ResponseEntity.ok("Emergency detected! Elevator requested at floor: " + floorRequest.getRequestedFloor());
        } catch (MinFloorException | MaxFloorException e) {
            return ResponseEntity.badRequest().body("Failed to request elevator..");
        }
    }

}
