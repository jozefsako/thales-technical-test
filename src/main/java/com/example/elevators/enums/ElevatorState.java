package com.example.elevators.enums;

public enum ElevatorState {
    MOVING, STOPPED, IDLE;

    @Override
    public String toString() {
        switch (this) {
            case MOVING:
                return "Moving";

            case STOPPED:
                return "Stopped";

            case IDLE:
                return "Idle";

            default:
                return "Unknown";
        }
    }
}
