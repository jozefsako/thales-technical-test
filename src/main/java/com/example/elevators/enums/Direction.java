package com.example.elevators.enums;

public enum Direction {
    UP, DOWN;

    @Override
    public String toString() {
        switch (this) {
            case UP:
                return "Up";

            case DOWN:
                return "Down";

            default:
                return "Unknown";
        }
    }
}
