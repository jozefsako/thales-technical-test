package com.example.elevators.models;

import com.example.elevators.enums.Direction;
import com.example.elevators.exceptions.MaxFloorException;
import com.example.elevators.exceptions.MinFloorException;

public interface IElevator extends Runnable {
    /**
     * Allows user inside the elevator to request the
     * destination floor.
     *
     * @param floor: destination floor.
     * @param isEmergency: defines emergency.
     *
     * @throws MinFloorException: if tries to go below min floor.
     * @throws MaxFloorException: if tries to go over max floor.
     */
    void dispatchElevator(int floor, boolean isEmergency) throws MinFloorException, MaxFloorException;

    /**
     * Allows user to request the elevator at his current floor.
     *
     * @param floorRequest: that contains, the floor number, direction and emergency.
     *
     * @throws MinFloorException: if tries to go below min floor.
     * @throws MaxFloorException: if tries to go over max floor.
     */
    void requestElevator(FloorRequest floorRequest) throws MinFloorException, MaxFloorException;
}
