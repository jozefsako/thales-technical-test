package com.example.elevators.models;

import com.example.elevators.enums.Direction;
import lombok.Getter;

@Getter
public class FloorRequest {

    private final int requestedFloor;
    private final Direction direction;
    private final boolean emergency;

    public FloorRequest(int floor, Direction direction, boolean emergency){
        this.requestedFloor = floor;
        this.direction = direction;
        this.emergency = emergency;
    }
}
