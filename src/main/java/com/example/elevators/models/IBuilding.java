package com.example.elevators.models;


import com.example.elevators.enums.Direction;
import com.example.elevators.exceptions.MaxFloorException;
import com.example.elevators.exceptions.MinFloorException;

public interface IBuilding {
    /**
     * Select the most appropriate elevator based on the user's
     * current floor and direction.
     *
     * @param floorRequest: that contains the floor, direction and emergency.
     *
     * @throws MinFloorException: if tries to go below min floor.
     * @throws MaxFloorException: if tries to go over max floor.
     */
    void requestElevator(FloorRequest floorRequest) throws MinFloorException, MaxFloorException;
}
