package com.example.elevators.models;

import com.example.elevators.enums.Direction;
import com.example.elevators.exceptions.MaxFloorException;
import com.example.elevators.exceptions.MinFloorException;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Component
public class Building implements IBuilding {

    private static final int DEFAULT_FLOORS = 10;
    private static final int DEFAULT_ELEVATORS = 3;
    private static final int DEFAULT_ELEVATOR_VELOCITY = 1500;

    private final int floors;
    private final List<Elevator> listElevators;

    private static Building INSTANCE;

    public static Building getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Building();
        }
        return INSTANCE;
    }

    private Building() {
        this.floors = DEFAULT_FLOORS;
        this.listElevators = new ArrayList<>(DEFAULT_ELEVATORS);
        initElevators();
    }

    private void initElevators() {
        for (int i = 0; i < DEFAULT_ELEVATORS; i++) {
            Elevator e = new Elevator(floors, DEFAULT_ELEVATOR_VELOCITY);
            this.listElevators.add(e);
            new Thread(e).start();
        }
    }

    public void requestElevator(FloorRequest floorRequest) throws MinFloorException, MaxFloorException {
        Elevator elevator = findOptimalElevator(floorRequest.getRequestedFloor(), floorRequest.getDirection()).get(0);
        elevator.requestElevator(floorRequest);
    }

    private List<Elevator> findOptimalElevator(int floor, Direction direction) {
        return listElevators.stream()
                .sorted(Comparator.comparingInt(elevator -> Math.abs(elevator.getCurrentFloor() - floor)))
                .filter(elevator -> elevator.getDirection() == direction || elevator.isFree())
                .collect(Collectors.toList());
    }
}
