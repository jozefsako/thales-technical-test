package com.example.elevators.models;

import com.example.elevators.enums.Direction;
import com.example.elevators.enums.ElevatorState;
import com.example.elevators.exceptions.MaxFloorException;
import com.example.elevators.exceptions.MinFloorException;
import lombok.Getter;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

@Getter
public class Elevator implements IElevator {

    private static final int DEFAULT_FLOOR = 0;
    private static final int DEFAULT_VELOCITY = 1500;
    private static final int DEFAULT_STOPPING_TIME = 2000;

    private final int velocity;
    private final int floors;
    private final Queue<Integer> emergencyQueue = new PriorityQueue<>();
    private final Queue<Integer> pendingUpQueue = new PriorityQueue<>();
    private final Queue<Integer> pendingDownQueue = new PriorityQueue<>(Comparator.reverseOrder());

    private int currentFloor = DEFAULT_FLOOR;
    private Direction direction = Direction.UP;
    private ElevatorState elevatorState = ElevatorState.IDLE;

    public Elevator(int floors, int velocity) {
        this.floors = floors;
        this.velocity = velocity > 0 ? velocity : DEFAULT_VELOCITY;
    }

    public void dispatchElevator(int floor, boolean isEmergency) throws MinFloorException, MaxFloorException {
        verifyFloor(floor);

        if (isEmergency) {
          emergencyQueue.add(floor);
        } else if (currentFloor < floor) {
            pendingUpQueue.add(floor);
        } else {
            pendingDownQueue.add(floor);
        }
    }

    public void requestElevator(FloorRequest floorRequest) throws MinFloorException, MaxFloorException {
        verifyFloor(floorRequest.getRequestedFloor());
        if (movingUp()) {
            pendingUpQueue.add(floorRequest.getRequestedFloor());
        } else {
            pendingDownQueue.add(floorRequest.getRequestedFloor());
        }
    }

    private void verifyFloor(int requestedFloor) throws MaxFloorException, MinFloorException {
        if (requestedFloor > floors) {
            throw new MaxFloorException();
        }
        if (requestedFloor < 0) {
            throw new MinFloorException();
        }
    }

    public boolean isFree() {
        return pendingUpQueue.isEmpty() && pendingDownQueue.isEmpty() && emergencyQueue.isEmpty();
    }

    public boolean isEmergency(){
        return !this.emergencyQueue.isEmpty();
    }

    private boolean movingUp() {
        return this.direction == Direction.UP;
    }

    private boolean movingDown() {
        return this.direction == Direction.DOWN;
    }

    private void idle() {
        elevatorState = ElevatorState.IDLE;
    }

    private void openDoors() throws InterruptedException {
        elevatorState = ElevatorState.STOPPED;
        System.out.println("Opening doors at floor " + currentFloor);
        Thread.sleep(DEFAULT_STOPPING_TIME);
        System.out.println("Closing doors");
    }

    private void resetDirection() {
        if (isFree()) {
            direction = Direction.UP;
        } else if(movingUp() && pendingUpQueue.isEmpty()) {
            this.direction = Direction.DOWN;
        } else if(movingDown() && pendingDownQueue.isEmpty()) {
            this.direction = Direction.UP;
        }
    }

    private void move() throws InterruptedException {
        Queue<Integer> queue = isEmergency() ? emergencyQueue : movingUp() ? pendingUpQueue : pendingDownQueue;

        if(!queue.isEmpty()) {
            this.elevatorState = ElevatorState.MOVING;
            int floor = queue.peek();

            if (floor == currentFloor) {
                openDoors();
                queue.poll();
            } else if (queue.contains(currentFloor)) {
                openDoors();
                queue.remove(currentFloor);
            }

            int direction = Integer.compare(floor, currentFloor);
            currentFloor += direction;
        }
        resetDirection();
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (!isFree()) {
                    move();
                } else {
                    idle();
                }
                Thread.sleep(velocity);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}